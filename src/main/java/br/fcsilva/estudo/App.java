package br.fcsilva.estudo;

import java.util.GregorianCalendar;

import br.fcsilva.estudo.state.ContratoContexto;

public class App {

	public static void main(String[] args) {
		ContratoContexto c = new ContratoContexto(1, 
				"12345",
				new GregorianCalendar(22, 05, 2015), 
				new GregorianCalendar(11,02, 2016), 
				new GregorianCalendar(11,02, 2016));

		System.out.println(c.getSituacao());
	}
}
