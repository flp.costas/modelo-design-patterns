package br.fcsilva.estudo.state;

public class EncerradoSituacaoState implements SituacaoState {

	public void estaVigente(ContratoContexto contexto) {
		contexto.setEstado(new VigenteSituacaoState());
		contexto.estaVigente();
	}

	public void estaEncerrado(ContratoContexto contexto) {
		if (contexto.getDataFim() != null) {
			contexto.setSituacao(SituacaoContratoEnum.ENCERRADO);
			contexto.setEstado(this);
		}
	}

}
