package br.fcsilva.estudo.state;

import java.util.Calendar;

/**
 * Contrato
 * 
 * @author filipe
 * 
 *         Situacao do contrato:
 * 
 *         VIGENTE: data atual > data inicio e data atual < data prevista fim e data fim nula
 *         ENCERRADO: data fim preenchida
 *         INICIAR: data atual < data inicio
 *
 */
public class ContratoContexto {

	private int id;
	private String numero;
	private SituacaoContratoEnum situacao;
	private Calendar dataInicio;
	private Calendar dataPrevistaFim;
	private Calendar dataFim;
	private SituacaoState state;
	
	
	public ContratoContexto(int id, String numero, Calendar dataInicio, Calendar dataPrevistaFim, Calendar dataFim) {
		this.id = id;
		this.numero= numero;
		this.dataInicio = dataInicio;
		this.dataPrevistaFim = dataPrevistaFim;
		this.dataFim = dataFim;
		setEstado(new VigenteSituacaoState());
		estaVigente();
		estaEncerrado();
	}
	
	public void setEstado(SituacaoState state){
		this.state = state;
	}
	
	public void estaVigente() {
		state.estaVigente(this);
	}
	
	public void estaEncerrado() {
		state.estaEncerrado(this);
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public SituacaoContratoEnum getSituacao() {
		return situacao;
	}

	public void setSituacao(SituacaoContratoEnum situacao) {
		this.situacao = situacao;
	}

	public Calendar getDataInicio() {
		return dataInicio;
	}

	public void setDataInicio(Calendar dataInicio) {
		this.dataInicio = dataInicio;
	}

	public Calendar getDataPrevistaFim() {
		return dataPrevistaFim;
	}

	public void setDataPrevistaFim(Calendar dataPrevistaFim) {
		this.dataPrevistaFim = dataPrevistaFim;
	}

	public Calendar getDataFim() {
		return dataFim;
	}

	public void setDataFim(Calendar dataFim) {
		this.dataFim = dataFim;
	}
}
