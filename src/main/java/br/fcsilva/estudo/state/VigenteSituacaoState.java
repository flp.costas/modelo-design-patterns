package br.fcsilva.estudo.state;


public class VigenteSituacaoState implements SituacaoState {

	public void estaVigente(ContratoContexto contexto) {
		if (contexto.getDataFim() == null) {
			contexto.setSituacao(SituacaoContratoEnum.VIGENTE);
			contexto.setEstado(this);
		}
	}

	public void estaEncerrado(ContratoContexto contexto) {
		contexto.setEstado(new EncerradoSituacaoState());
		contexto.estaEncerrado();
	}

}
