package br.fcsilva.estudo.state;

public interface SituacaoState {

	void estaVigente(ContratoContexto contexto);
	void estaEncerrado(ContratoContexto contexto);
}
